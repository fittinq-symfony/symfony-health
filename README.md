# Symfony health 
This helps creating health endpoints which simply return HTTP_OK responses.

## Install via composer 
```
composer require fittinq\symfony-health
```
## Configure bundle
config/bundles.php

```
<?php

return [
    // ...
    Fittinq\Symfony\Health\SymfonyHealthBundle::class => ['all' => true],
];
```

## Configure routes
config/routes.yaml
```
health:
    resource: "@SymfonyHealthBundle/Resources/config/routing.yaml"
```



